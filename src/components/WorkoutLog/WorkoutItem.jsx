import React, { PropTypes } from 'react';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import Dialog from 'material-ui/Dialog';
import moment from 'moment';
import { hoursToStr } from '../../util';

const propTypes = {
  id: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  onWorkoutDelete: PropTypes.func.isRequired,
};

class WorkoutItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = { open: false };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleDelete() {
    this.props.onWorkoutDelete(this.props.id);
  }

  render() {
    const date = moment(this.props.date).format('DD/MM/YYYY');
    const time = hoursToStr(this.props.time);

    const actions = [
      <FlatButton
        label="No"
        primary
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Yes"
        secondary
        keyboardFocused
        onTouchTap={this.handleDelete}
      />,
    ];

    return (
      <TableRow className="Workout-listItem">
        <TableRowColumn>{time}</TableRowColumn>
        <TableRowColumn>{this.props.type}</TableRowColumn>
        <TableRowColumn>{date}</TableRowColumn>
        <TableRowColumn rowNumber={this.props.id}>
          <IconButton onTouchTap={this.handleOpen}>
            <ActionDelete />
            <Dialog
              title="Delete"
              actions={actions}
              modal={false}
              open={this.state.open}
              onRequestClose={this.handleClose}
            >
              Are you sure you want to delete this item?
            </Dialog>
          </IconButton>
        </TableRowColumn>
      </TableRow>
    );
  }
}

WorkoutItem.propTypes = propTypes;

export default WorkoutItem;
