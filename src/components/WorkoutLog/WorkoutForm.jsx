import React, { PropTypes } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';

const propTypes = {
  onWorkoutSubmit: PropTypes.func.isRequired,
};

class WorkoutForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { notify: false, time: {}, type: '', date: '' };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  handleTimeChange(e, time) {
    this.setState({ time });
  }

  handleTypeChange(e, index, value) {
    this.setState({ type: value });
  }

  handleDateChange(e, date) {
    this.setState({ date });
  }

  handleSubmit(e) {
    e.preventDefault();

    if (!this.state.time || !this.state.type || !this.state.date) {
      return;
    }

    const time = moment(this.state.time).hour() +
      moment(this.state.time).minute() / 60;
    const type = this.state.type.trim();
    const date = moment(this.state.date).format('YYYY-MM-DD');

    this.props.onWorkoutSubmit({ time, type, date });

    this.setState({ time: {}, type: '', date: '' });
  }

  render() {
    return (
      <form className="Workout-form" onSubmit={this.handleSubmit}>
        <TimePicker
          value={this.state.time}
          hintText="Time"
          format="24hr"
          onChange={this.handleTimeChange}
        />
        <SelectField
          value={this.state.type}
          hintText="Type"
          floatingLabelText="Type"
          floatingLabelFixed
          style={{ marginTop: '-25px' }}
          onChange={this.handleTypeChange}
        >
          <MenuItem value="Running" primaryText="Running" />
          <MenuItem value="Swimming" primaryText="Swimming" />
          <MenuItem value="Dancing" primaryText="Dancing" />
          <MenuItem value="Walking" primaryText="Walking" />
          <MenuItem value="Bikink" primaryText="Biking" />
        </SelectField>
        <DatePicker
          value={this.state.date}
          hintText="Date"
          formatDate={(date) => moment(date).format('DD/MM/YYYY')}
          onChange={this.handleDateChange}
        />
        <RaisedButton
          label="Add"
          onClick={this.handleSubmit}
        />
      </form>
    );
  }
}

WorkoutForm.propTypes = propTypes;

export default WorkoutForm;
