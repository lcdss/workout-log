import React from 'react';
import { remove } from 'lodash/array';
import { maxBy } from 'lodash/math';
import { merge } from 'lodash/object';
import Paper from 'material-ui/Paper';
import Snackbar from 'material-ui/Snackbar';

import WorkoutForm from './WorkoutForm';
import WorkoutList from './WorkoutList';

const workouts = [
  { id: 1, time: 1, type: 'Running', date: '2016-01-01' },
  { id: 2, time: 0.5, type: 'Swimming', date: '2016-01-02' },
  { id: 3, time: 3, type: 'Biking', date: '2016-01-03' },
  { id: 4, time: 1.2, type: 'Running', date: '2016-01-04' },
  { id: 5, time: 2, type: 'Football', date: '2016-01-05' },
  { id: 6, time: 1, type: 'Dancing', date: '2016-01-06' },
  { id: 7, time: 3, type: 'Walking', date: '2016-01-07' },
  { id: 8, time: 1, type: 'Aerobic', date: '2016-01-08' },
];

class WorkoutLog extends React.Component {
  constructor(props) {
    super(props);

    this.state = { notifyNew: false, notifyDelete: false, workouts };

    this.handleWorkoutSubmit = this.handleWorkoutSubmit.bind(this);
    this.handleWorkoutDelete = this.handleWorkoutDelete.bind(this);
    this.handleNotifyNewOpen = this.handleNotifyNewOpen.bind(this);
    this.handleNotifyNewClose = this.handleNotifyNewClose.bind(this);
    this.handleNotifyDeleteOpen = this.handleNotifyDeleteOpen.bind(this);
    this.handleNotifyDeleteClose = this.handleNotifyDeleteClose.bind(this);
  }

  handleNotifyNewOpen() {
    this.setState({ notifyNew: true });
  }

  handleNotifyNewClose() {
    this.setState({ notifyNew: false });
  }

  handleNotifyDeleteOpen() {
    this.setState({ notifyDelete: true });
  }

  handleNotifyDeleteClose() {
    this.setState({ notifyDelete: false });
  }

  handleWorkoutSubmit(workout) {
    const maxId = (maxBy(this.state.workouts, item => item.id)).id + 1;
    this.state.workouts.push(merge({ id: maxId }, workout));
    this.setState({ workouts: this.state.workouts });
    this.handleNotifyNewOpen();
  }

  handleWorkoutDelete(id) {
    remove(this.state.workouts, item => item.id === id);
    this.setState({ workouts: this.state.workouts });
    this.handleNotifyDeleteOpen();
  }

  render() {
    return (
      <Paper zDepth={1}>
        <div className="Workout">
          <WorkoutForm onWorkoutSubmit={this.handleWorkoutSubmit} />
          <WorkoutList
            workouts={this.state.workouts}
            onWorkoutDelete={this.handleWorkoutDelete}
          />
          <Snackbar
            open={this.state.notifyNew}
            message="Workout item successfully added to your list!"
            autoHideDuration={5000}
            onRequestClose={this.handleNotifyNewClose}
            bodyStyle={{ backgroundColor: '#3c763d' }}
          />
          <Snackbar
            open={this.state.notifyDelete}
            message="Workout item successfully deleted from your list!"
            autoHideDuration={5000}
            onRequestClose={this.handleNotifyDeleteClose}
            bodyStyle={{ backgroundColor: '#a94442' }}
          />
        </div>
      </Paper>
    );
  }
}

export default WorkoutLog;
