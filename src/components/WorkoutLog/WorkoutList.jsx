import React, { PropTypes } from 'react';
import sumBy from 'lodash/sumBy';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { hoursToStr } from '../../util';

import WorkoutItem from './WorkoutItem';

const propTypes = {
  workouts: PropTypes.arrayOf(PropTypes.object).isRequired,
  onWorkoutDelete: PropTypes.func.isRequired,
};

class WorkoutList extends React.Component {
  renderWorkouts() {
    return this.props.workouts.map((item) => (
      <WorkoutItem
        key={item.id}
        id={item.id}
        time={item.time}
        type={item.type}
        date={item.date}
        onWorkoutDelete={this.props.onWorkoutDelete}
      />
    ));
  }

  render() {
    const time = hoursToStr(sumBy(this.props.workouts, item => item.time));

    return (
      <Table className="Workout-list" selectable={false}>
        <TableHeader displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn>Time</TableHeaderColumn>
            <TableHeaderColumn>Type</TableHeaderColumn>
            <TableHeaderColumn>Date</TableHeaderColumn>
            <TableHeaderColumn>Options</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
          {this.renderWorkouts()}
        </TableBody>
        <TableFooter className="Workout-list-footer">
          <TableRow>
            <TableRowColumn>{time} of exercises</TableRowColumn>
          </TableRow>
        </TableFooter>
      </Table>
    );
  }
}

WorkoutList.propTypes = propTypes;

export default WorkoutList;
