import React from 'react';

import WorkoutLog from './WorkoutLog';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-header-text">Workout Log</h1>
      </header>
      <WorkoutLog />
    </div>
  );
}

export default App;
