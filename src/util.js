import { floor, round } from 'lodash/math';

function hoursToStr(number) {
  const hours = floor(number);
  const minutes = round((number - hours) * 60);

  return `${hours ? `${hours}h` : ''}
    ${minutes ? ` ${minutes}m` : ''}`;
}

export { hoursToStr };
