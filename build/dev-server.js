/* eslint-disable */
var path = require('path');
var config = require('../config');
var express = require('express');
var webpack = require('webpack');
var webpackConfig = require('./webpack.dev.conf');

var PORT = process.env.PORT || config.dev.port;
var staticPath = path.join(config.build.assetsPublicPath, config.build.assetsSubDirectory);

var app = express();
var compiler = webpack(webpackConfig);

var devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: webpackConfig.output.publicPath,
  stats: {
    colors: true,
    chunks: false,
  },
});

var hotMiddleware = require('webpack-hot-middleware')(compiler);

compiler.plugin('compilation', function (compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
    hotMiddleware.publish({ action: 'reload' });
    cb();
  });
});

app.use(devMiddleware);

app.use(hotMiddleware);

app.use(staticPath, express.static('./static'));

module.exports = app.listen(PORT, function (err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://localhost:' + PORT + '\n');
});
