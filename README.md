# Workout Log
Workout Log is a example of how to use React + Webpack + SASS + PostCSS (Autoprefixer) together.

### Instructions
1. Download the last version and extract the file
2. Go to unzipped folder
3. Install dependencies with `$ npm install`
4. Serve with hot reload at localhost:8080 with `$ npm run dev`

#### Others Commands
- Build for production with `$ npm run build`

### Tools
- Babel
- React
- Webpack
- SASS
- Autoprefixer
- Eslint
- Express
- Lodash
- Moment
- Material-ui

### Code Style
- [Javascript / ES5 / React / CSS & SASS](https://github.com/airbnb/javascript)
- [HTML & CSS](http://codeguide.co/)
